#!/bin/python

# Include
import constants

# Local Constants


class SetCard(object):
    """The class that define what a card is."""

    # The form of the card
    form = None
    # The color of the card
    color = None
    # The fill of the card
    fill = None
    # The number of form of the card
    number = None

    def checkAndSetForm(self, form):
        """Check that the given form is a right one and set it.

        @param  form  (int)  The form to check and set.
        """
        if form not in constants.POSSIBLE_FORM_C:
            raise ValueError("The given form do not exist.")
        self.form = form

    def checkAndSetColor(self, color):
        """Check that the given color is a right one and set it.

        @param  color  (int)  The color to check and set.
        """
        if color not in constants.POSSIBLE_COLOR_C:
            raise ValueError("The given color do not exist.")
        self.color = color

    def checkAndSetFill(self, fill):
        """Check that the given fill is a right one and set it.

        @param  fill  (int)  The fill to check and set.
        """
        if fill not in constants.POSSIBLE_FILL_C:
            raise ValueError("The given fill do not exist.")
        self.fill = fill

    def checkAndSetNumber(self, number):
        """Check that the form given is a right one and set it.

        @param  number  (int)  The number to check and set.
        """
        if number not in constants.POSSIBLE_NUMBER_C:
            raise ValueError("The given number do not exist.")
        self.number = number

    def __init__(self, form, color, fill, number):
        """The constructor of a card

        @param  form    (int)  The form of the card.
        @param  color   (int)  The color of the card.
        @param  fill    (int)  The fill of the card.
        @param  number  (int)  The number of form of the card.
        """
        self.checkAndSetForm(form=form)
        self.checkAndSetColor(color=color)
        self.checkAndSetFill(fill=fill)
        self.checkAndSetNumber(number=number)

    def __str__(self):
        """The string representation of a card"""
        return "[{form}, {color}, {fill}, {number}]".format(
            form=self.form, color=self.color, fill=self.fill, number=self.number
        )

    def __repr__(self):
        """Return the string representation of a card"""
        return self.__str__()

    def __eq__(self, otherCard):
        """The comparison method for a card.

        @param  otherCard  (SetCard)  The card to compare with.

        return  (bool)  True if the two card have the same charateristics.
        """
        if not isinstance(otherCard, SetCard):
            raise TypeError("Can't compare with given argument")
        return (
            (self.form == otherCard.form)
            and (self.color == otherCard.color)
            and (self.fill == otherCard.fill)
            and (self.number == otherCard.number)
        )

    def __hash__(self):
        """The has method for a card"""
        # @HERE: not really sure that's the way I want to hash a card
        return hash(tuple([self.form, self.color, self.fill, self.number]))

    def determineMissingCard(self, otherCard):
        """Determine what is the third card needed to form a set

        @param  otherCard  (SetCard)  The second card of the expected set.

        @return  (SetCard)  The third card of the expected set.
        """
        return SetCard(
            form=-(self.form + otherCard.form) % 3,
            color=-(self.color + otherCard.color) % 3,
            fill=-(self.fill + otherCard.fill) % 3,
            number=-(self.number + otherCard.number) % 3,
        )
