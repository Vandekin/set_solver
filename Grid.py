#!/bin/python

# Imports
import SetCard
import constants


class Grid(object):
    """Class that manage the game grid."""

    # The list of card in the grid
    listOfCards = []

    # The list of the sets in the grid
    listOfSets = set()

    def checkAndAddCard(self, card):
        """Check that the given card is not already in the grid and add it

        @param  card  (SetCard)  The card to check and add.
        """
        if card in self.listOfCards:
            raise ValueError("This card is already in the grid.")
        self.listOfCards.append(card)

    def __init__(self, listOfCards):
        """The init method of the Grid class

        @param  listOfCards  (list of SetCard)  The cards to add.
        """
        if (
            len(listOfCards)
            != constants.BASE_GRID_LENGTH_C * constants.BASE_GRID_WIDTH_C
        ):
            raise ValueError("Not enough cards to set up a grid")
        for card in listOfCards:
            self.checkAndAddCard(card)

    def __str__(self):
        """The string representation of the grid

        @return  ret_str  (str)  The string of the grid.
        """
        ret_str = ""
        for xIndex in range(constants.BASE_GRID_WIDTH_C):
            for yIndex in range(constants.BASE_GRID_LENGTH_C):
                ret_str += (
                    str(
                        self.listOfCards[xIndex * constants.BASE_GRID_LENGTH_C + yIndex]
                    )
                    + "\t"
                )
            ret_str += "\n"
        return ret_str

    def __repr__(self):
        """Return the string representation of the grid"""
        return self.__str__()

    def solveGrid(self):
        """Find all the sets in the grid"""
        for card in self.listOfCards:
            for otherCard in self.listOfCards:
                missingCard = (
                    card.determineMissingCard(otherCard)
                    if self.listOfCards.index(otherCard) > self.listOfCards.index(card)
                    else None
                )
                if (missingCard is not None) and (missingCard in self.listOfCards):
                    self.listOfSets.update(set([card, otherCard, missingCard]))
