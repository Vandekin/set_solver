#!/bin/python

# Imports
import Grid
import SetCard


def launchSolver(exampleList):
    """Function that launch the solver with a given example gid.

    @param  exampleList  (list of tuple)  The list of all the card in the grid.
    """
    gameGrid = Grid.Grid(listOfCards=exampleList)
    print(gameGrid)
    gameGrid.solveGrid()
    print(gameGrid.listOfSets)


if __name__ == "__main__":
    # (form, color, fill, number)
    exampleList = [
        SetCard.SetCard(form=2, color=1, fill=2, number=1),
        SetCard.SetCard(form=2, color=2, fill=0, number=1),
        SetCard.SetCard(form=0, color=1, fill=2, number=2),
        SetCard.SetCard(form=1, color=0, fill=2, number=2),
        SetCard.SetCard(form=1, color=0, fill=0, number=2),
        SetCard.SetCard(form=1, color=2, fill=2, number=2),
        SetCard.SetCard(form=2, color=2, fill=1, number=1),
        SetCard.SetCard(form=1, color=2, fill=1, number=0),
        SetCard.SetCard(form=2, color=1, fill=1, number=0),
        SetCard.SetCard(form=0, color=0, fill=0, number=2),
        SetCard.SetCard(form=1, color=1, fill=0, number=2),
        SetCard.SetCard(form=1, color=0, fill=0, number=1),
    ]
    launchSolver(exampleList=exampleList)
