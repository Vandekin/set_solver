#!/bin/python

""" List of all the possible forms:
        0: OVAL
        1: WAVE
        2: DIAMOND
"""
POSSIBLE_FORM_C = [index for index in range(3)]

""" List of all the possible colors:
        0: RED
        1: GREEN
        2: BLUE
"""
POSSIBLE_COLOR_C = list(POSSIBLE_FORM_C)

""" List of all the possible fills:
        0: SOLID
        1: EMPTY
        2: HATCHED
"""
POSSIBLE_FILL_C = list(POSSIBLE_FORM_C)

""" List of all the possible number of forms:
        0: ONE
        1: TWO
        2: THREE
"""
POSSIBLE_NUMBER_C = list(POSSIBLE_FORM_C)

# The length of the grid (in cards)
BASE_GRID_LENGTH_C = 4
# The width of the grid (in cards)
BASE_GRID_WIDTH_C = 3
